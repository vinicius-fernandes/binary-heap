/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   BinaryTree.h
 * Author: vinicius
 *
 * Created on 14 de Maio de 2018, 19:17
 */

#ifndef BINARYTREE_H
#define BINARYTREE_H

#include <iostream>
#include <stddef.h>

#include "NodeTree.h"

using namespace std;

class BinaryTree {
public:

    /**
     * Constructor: Builds a binary tree. root arrow as null.
     */
    BinaryTree();

    /**
     * Constructor: Builds a binary tree
     * 
     * @param key to set
     */
    BinaryTree(int key);

    BinaryTree(const BinaryTree& orig);

    /**
     * Destroyer binary tree. Automatically clears the allocated memory space
     * for the binary tree.
     */
    virtual ~BinaryTree();

    /**
     * root node of the tree to set.
     * 
     * @param root node. 
     */
    void SetRoot(NodeTree* root);

    /**
     * @return current root node of the tree.
     */
    NodeTree* GetRoot() const;

    /**
     * @return true if the tree is empty (root = NULL)
     * or false if the tree has nodes (root != NULL).
     */
    bool Empty();

    /**
     * 
     * 
     * @param key
     * @param ptRoot
     * 
     * @return 
     */
    int Search(int key, NodeTree*& ptRoot);

    /**
     * 
     * 
     * @param key
     * @param ṕtRoot
     */
    bool Insert(int key, NodeTree*& ptRoot);

    /**
     * runs the tree in preorder, started from the root.
     * 
     * @param tree root.
     */
    void PreOrder(NodeTree* root);

    /**
     * 
     * 
     * @param root
     */
    void InOrder(NodeTree* root);

    /**
     * visit the node passed by parameter, displaying its key.
     * 
     * @param tree root.
     */
    void Visit(NodeTree* root);

private:
    NodeTree* root; // root node of the tree.
};

#endif /* BINARYTREE_H */

