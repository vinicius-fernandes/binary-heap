/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   NodeTree.cpp
 * Author: vinicius
 * 
 * Created on 14 de Maio de 2018, 19:12
 */

#include "NodeTree.h"

/**
 * Constructor Default: Builds a node
 */
NodeTree::NodeTree() {
    this->left = NULL;
    this->right = NULL;
}

/**
 * Constructor: Builds a node
 * 
 * @param key to set
 */
NodeTree::NodeTree(int key) {
    this->key = key;
    this->left = NULL;
    this->right = NULL;
}

NodeTree::NodeTree(const NodeTree& orig)
{
}


/**
 * Destroyer node. Automatically clears the allocated memory space for the node.
 */
NodeTree::~NodeTree() {
    delete this->left;
    delete this->right;
}

/**
 * key to set
 * 
 * @param key 
 */
void NodeTree::SetKey(int key) {
    this->key = key;
}

/**
 * @return current key
 */
int NodeTree::GetKey() const {
    return this->key;
}

/**
 * node to the right of the root to set
 * 
 * @param node right
 */
void NodeTree::SetRight(NodeTree* right) {
    this->right = right;
}

/**
 * @return node to the right of the root.
 */
NodeTree* NodeTree::GetRight() const {
    return this->right;
}

/**
 * node to the left of the root to set
 * 
 * @param node left
 */
void NodeTree::SetLeft(NodeTree* left) {
    this->left = left;
}

/**
 * @return node to the left of the root.
 */
NodeTree* NodeTree::GetLeft() const {
    return this->left;
}

