/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Node.h
 * Author: vinicius
 *
 * Created on 11 de Maio de 2018, 18:20
 */

#ifndef NODE_H
#define NODE_H

class NodeBinaryHeap {
public:

    /**
     * Constructor: Builds a node from a given
     * 
     * @param key
     */
    NodeBinaryHeap(int key);

    /**
     * Destroyer node
     */
    virtual ~NodeBinaryHeap();

    /**
     * key to set
     * 
     * @param key 
     */
    void SetKey(int key);

    /**
     * @return current key
     */
    int GetKey() const;

private:
    int key; // priority key of a node
};

#endif /* NODE_H */

