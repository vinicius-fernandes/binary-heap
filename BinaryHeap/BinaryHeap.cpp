/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   BinaryHeap.cpp
 * Author: vinicius
 * 
 * Created on 11 de Maio de 2018, 18:25
 */

#include "BinaryHeap.h"

/**
 * Constructor: Builds a heap
 * 
 * @param heap capacity. Enter a positive value that will be the maximum 
 * capacity of the heap.
 */
BinaryHeap::BinaryHeap(int capacity) {
    this->N = 0;
    this->capacity = capacity;
    this->heap = new NodeBinaryHeap*[capacity];

    /* initializes index 0 from Heap. INT_MAX defines a constant with the limit 
     * value of the integer type.*/
    NodeBinaryHeap* node = new NodeBinaryHeap(INT_MAX);
    this->heap[0] = node;
}

/**
 * Destroyer heap. Automatically clears the allocated memory space for the heap.
 */
BinaryHeap::~BinaryHeap() {
    delete this->heap;
}

/**
 * @return the size of the Heap, that is, the number of nodes
 * inserted in the Heap.
 */
int BinaryHeap::Size() {
    return this->N;
}

/**
 * @return the maximum number of nodes the Heap can receive.
 */
int BinaryHeap::HeapCapacity() {
    return this->capacity;
}

/** 
 * @return information(its key) from the highest priority node
 * of the Heap. case n == 0. returns - 1 indicating that heap is empty
 * (heap[1] == NULL).
 */
int BinaryHeap::Select() {

    if (this->Size() != 0)
        return this->heap[1]->GetKey();
    else
        return -1;
}

/**
 * removes the higher priority node from the Heap.
 * 
 * @return returning its information (your key priority). 
 * Case n == 0. returns -1 indicating that heap is empty (heap[1] == NULL)
 */
int BinaryHeap::Remove() {
    int key;

    if (this->Size() != 0) {

        key = this->heap[1]->GetKey();
        this->heap[1] = this->heap[this->Size()];
        this->N--;

        this->MoveDown(1, this->Size());
        return key;
    }
    return -1;
}

/**
 * inserts a new node in the Heap, only if it does not already exist.
 * 
 * @param key that will be inserted.
 * 
 * @return true (case n < the capacity of the heap and
 * the key does not exist in the Heap), or false (indicating n >= the
 * capacity of the heap or the key is already in the Heap).
 */
bool BinaryHeap::Insert(int key) {

    if (this->Size() < this->HeapCapacity()) {

        if (this->Search(key) == -1) {

            NodeBinaryHeap* node = new NodeBinaryHeap(key);

            this->N++;
            this->heap[this->Size()] = node;
            this->MoveUp(this->Size());
        }

    } else
        return false;

    return true;
}

/**
 * search for a key specific node in the Heap.
 * 
 * @param the key you want to fetch in the heap.
 * @return key or -1 indicating that he did not find.
 */
int BinaryHeap::Search(int key) {

    for (int i = 1; i <= this->Size(); i++)

        if (key == this->heap[i]->GetKey())
            return this->heap[i]->GetKey();

    return -1;
}

/* 
 * @param old_key (the node key that will be changed).
 * @param new_key (the new key value that node).
 * 
 * @return true if the key you want to change exists inside the heap.
 * or false otherwise.f
 */
bool BinaryHeap::Update(int old_key, int new_key) {

    for (int i = 1; i <= this->Size(); i++)

        if (old_key == this->heap[i]->GetKey()) {
            this->heap[i]->SetKey(new_key);

            if (old_key < new_key)
                this->MoveUp(i);
            else
                this->MoveDown(i, this->Size());

            return true;
        }
    return false;
}

/**
 * receives as input a vector containing nodes organizes its elements
 * of according to the properties of a Heap.
 * 
 * @param vector
 * @param n (vector size)
 * 
 * @return true if the vector size (n) is smaller than the heap capacity.
 */
bool BinaryHeap::Heapify(int vector[], int n) {

    if (n < (this->HeapCapacity() - this->Size())) {

        for (int i = 0; i < n; i++)
            this->Insert(vector[i]);

    } else
        return false;

    return true;
}

/**
 * prints the Heap on the screen. if the heap is empty. prints 'Empty Heap'.
 */
void BinaryHeap::PrintHeap() {

    if (this->Size() != 0) {
        cout << "[";
        for (int i = 1; i <= this->Size(); i++) {
            cout << this->heap[i]->GetKey();

            if (i + 1 <= this->Size())
                cout << ", ";
        }
        cout << "]";
    } else
        cout << "Heap Empty.";
}

/**
 * receives as input a complete binary tree and converts it into a BinaryHeap
 */
void BinaryHeap::BinaryTreeToHeap(NodeTree* root) {

    if (root != NULL) {

        this->Insert(root->GetKey());

        this->BinaryTreeToHeap(root->GetLeft());
        this->BinaryTreeToHeap(root->GetRight());
    }
}

/**
 * convert a Binary Heap to a complete binary tree.
 * @return returns a BinaryTree containing the keys of the input vector.
 */
BinaryTree* BinaryHeap::HeapToBinaryTree() {
    this->tree = new BinaryTree();

    NodeTree *root = tree->GetRoot();
    for (int i = 1; i <= this->Size(); i++) {
        root = tree->GetRoot();
        tree->Insert(this->heap[i]->GetKey(), root);
    }
    return this->tree;
}

/**
 * modifies the current capacity of the Heap. Heap's ability can either 
 * be increased or decreased. If the new capacity desired is less than the 
 * number of nodes present in the list, the Heap capacity should not be
 * modified.

 * @param new heap capacity. If the value informed is negative (<0), the
 *  new capacity will be decremented, if positive it will be increased (>0)
 * 
 * @return false if the new capacity is less than the heap size (number of
 * nodes inserted).
 */
bool BinaryHeap::ChangeCapacity(int capacity) {
    NodeBinaryHeap** h = NULL;

    if (capacity > 0) {

        int increment = (this->HeapCapacity() + capacity);
        h = (NodeBinaryHeap**) realloc(this->heap,
                increment * sizeof (NodeBinaryHeap*));

        this->heap = h;
        this->capacity += capacity;
    } else {

        int decrement = ((this->HeapCapacity()) + (capacity));
        if (decrement > this->Size()) {

            h = (NodeBinaryHeap**) realloc(this->heap,
                    decrement * sizeof (NodeBinaryHeap*));
            this->heap = h;
            this->capacity += capacity;
        } else
            return false;
    }
    return true;
}

void BinaryHeap::SetTree(BinaryTree* tree) {
    this->tree = tree;
}

BinaryTree* BinaryHeap::GetTree() const {
    return this->tree;
}

/**
 * algorithm to move up to the BinaryHeap
 * 
 * @param i (current heap index)
 */
void BinaryHeap::MoveUp(int i) {
    int parent = (i / 2);

    if (parent >= 1) {

        if (this->heap[i]->GetKey() > this->heap[parent]->GetKey()) {
            Swap(i, parent);
            MoveUp(parent);
        }
    }
}

/**
 * algorithm to move down to the BinaryHeap
 * 
 * @param i (current heap index)
 * @param n (heap size)
 */
void BinaryHeap::MoveDown(int i, int n) {
    int child = (2 * i);

    if (child <= n) {

        if (child < n) {
            if (this->heap[child + 1]->GetKey() > this->heap[child]->GetKey())
                child++;
        }

        if (this->heap[i]->GetKey() < this->heap[child]->GetKey()) {
            this->Swap(i, child);
            this->MoveDown(child, n);
        }
    }
}

/**
 * A utility function to swap two elements.
 * 
 * @param x
 * @param y
 */
void BinaryHeap::Swap(int x, int y) {
    int temp = this->heap[x]->GetKey();
    this->heap[x]->SetKey(this->heap[y]->GetKey());
    this->heap[y]->SetKey(temp);
}

