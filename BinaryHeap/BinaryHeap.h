/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   BinaryHeap.h
 * Author: vinicius
 *
 * Created on 11 de Maio de 2018, 18:25
 */

#ifndef BINARYHEAP_H
#define BINARYHEAP_H

#include <iostream>
#include <cstdlib>
#include <climits>

#include "NodeBinaryHeap.h"
#include "BinaryTree.h"

using namespace std;

class BinaryHeap {
public:

    /**
     * Constructor: Builds a heap
     * 
     * @param heap capacity. Enter a positive value that will be the maximum 
     * capacity of the heap.
     */
    BinaryHeap(int capacity);

    /**
     * Destroyer heap. Automatically clears the allocated memory space for the heap.
     */
    virtual ~BinaryHeap();

    /**
     * @return the size of the Heap, that is, the number of nodes
     * inserted in the Heap.
     */
    int Size();

    /**
     * @return the maximum number of nodes the Heap can receive.
     */
    int HeapCapacity();

    /** 
     * @return information(its key) from the highest priority node
     * of the Heap. case n == 0. returns - 1 indicating that heap is empty
     * (heap[1] == NULL).
     */
    int Select();

    /**
     * removes the higher priority node from the Heap.
     * 
     * @return returning its information (your key priority). 
     * Case n == 0. returns -1 indicating that heap is empty (heap[1] == NULL)
     */
    int Remove();

    /**
     * inserts a new node in the Heap, only if it does not already exist.
     * 
     * @param key that will be inserted.
     * 
     * @return true (case n < the capacity of the heap and
     * the key does not exist in the Heap), or false (indicating n >= the
     * capacity of the heap or the key is already in the Heap).
     */
    bool Insert(int key);

    /**
     * search for a key specific node in the Heap.
     * 
     * @param the key you want to fetch in the heap.
     * @return key or -1 indicating that he did not find.
     */
    int Search(int key);

    /* 
     * @param old_key (the node key that will be changed).
     * @param new_key (the new key value that node).
     * 
     * @return true if the key you want to change exists inside the heap.
     * or false otherwise.f
     */
    bool Update(int old_key, int new_key);

    /**
     * receives as input a vector containing nodes organizes its elements
     * of according to the properties of a Heap.
     * 
     * @param vector
     * @param n (vector size)
     * 
     * @return true if the vector size (n) is smaller than the heap capacity.
     */
    bool Heapify(int vector[], int n);

    /**
     * prints the Heap on the screen. if the heap is empty. prints 'Empty Heap'.
     */
    void PrintHeap();

    /**
     * receives as input a complete binary tree and converts it into a BinaryHeap
     */
    void BinaryTreeToHeap(NodeTree* root);

    /**
     * convert a Binary Heap to a complete binary tree.
     * @return returns a BinaryTree containing the keys of the input vector.
     */
    BinaryTree* HeapToBinaryTree();

    /**
     * modifies the current capacity of the Heap. Heap's ability can either 
     * be increased or decreased. If the new capacity desired is less than the 
     * number of nodes present in the list, the Heap capacity should not be
     * modified.

     * @param new heap capacity. If the value informed is negative (<0), the
     *  new capacity will be decremented, if positive it will be increased (>0)
     * 
     * @return false if the new capacity is less than the heap size (number of
     * nodes inserted).
     */
    bool ChangeCapacity(int new_capacity);

    /**
     * tree to set
     * 
     * @param tree 
     */
    void SetTree(BinaryTree* tree);

    /**
     * @return current tree
     */
    BinaryTree* GetTree() const;

private:
    int N; // Current number of elements in heap (size)
    int capacity; // maximum possible size of heap
    NodeBinaryHeap** heap = NULL; // pointer to array of elements in heap
    BinaryTree* tree = NULL; // binary tree for conventions

    /**
     * algorithm to move up to the BinaryHeap
     * 
     * @param i (current heap index)
     */
    void MoveUp(int i);

    /**
     * algorithm to move down to the BinaryHeap
     * 
     * @param i (current heap index)
     * @param n (heap size)
     */
    void MoveDown(int i, int n);

    /**
     * A utility function to swap two elements (priorities).
     * 
     * @param x
     * @param y
     */
    void Swap(int i, int j);

};

#endif /* BINARYHEAP_H */

