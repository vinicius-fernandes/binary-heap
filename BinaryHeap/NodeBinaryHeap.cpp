/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Node.cpp
 * Author: vinicius
 * 
 * Created on 11 de Maio de 2018, 18:20
 */

#include "NodeBinaryHeap.h"

/**
 * Constructor: Builds a node from a given
 * 
 * @param key
 */
NodeBinaryHeap::NodeBinaryHeap(int key) {
    this->key = key;
}

/**
 * Destroyer node
 */
NodeBinaryHeap::~NodeBinaryHeap() {
}

/**
 * key to set
 * 
 * @param key 
 */
void NodeBinaryHeap::SetKey(int key) {
    this->key = key;
}

/**
 * @return current key
 */
int NodeBinaryHeap::GetKey() const {
    return key;
}

