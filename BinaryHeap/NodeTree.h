/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   NodeTree.h
 * Author: vinicius
 *
 * Created on 14 de Maio de 2018, 19:12
 */

#ifndef NODETREE_H
#define NODETREE_H

#include <stddef.h>

class NodeTree {
public:

    /**
     * Constructor Default: Builds a node
     */
    NodeTree();

    /**
     * Constructor: Builds a node
     * 
     * @param key to set
     */
    NodeTree(int key);
    
    NodeTree(const NodeTree& orig);

    /**
     * Destroyer node. Automatically clears the allocated memory space for the node.
     */
    virtual ~NodeTree();

    /**
     * key to set
     * 
     * @param key 
     */
    void SetKey(int key);

    /**
     * @return current key
     */
    int GetKey() const;

    /**
     * node to the right of the root to set
     * 
     * @param node right
     */
    void SetRight(NodeTree* right);

    /**
     * @return node to the right of the root.
     */
    NodeTree* GetRight() const;

    /**
     * node to the left of the root to set
     * 
     * @param node left
     */
    void SetLeft(NodeTree* left);

    /**
     * @return node to the left of the root.
     */
    NodeTree* GetLeft() const;

private:
    NodeTree* left; //node to the left of the root
    NodeTree* right; //node to the right of the root
    int key; // node key
};

#endif /* NODETREE_H */

